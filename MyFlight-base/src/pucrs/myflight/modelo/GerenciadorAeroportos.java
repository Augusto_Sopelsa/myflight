package pucrs.myflight.modelo;

import java.util.ArrayList;
import java.util.Collections;

public class GerenciadorAeroportos {
	private ArrayList<Aeroporto> Aeroportos;
	
	public GerenciadorAeroportos()
	{
		Aeroportos = new ArrayList<Aeroporto>();
	}
	
	public void AdicionarAeroportos(Aeroporto x)
	{
		Aeroportos.add(x);
	}
	
	public ArrayList<Aeroporto> ListarTodosAeroportos()
	{
		return Aeroportos;
	}
	
	public void ordenaAeroportos(){
		Collections.sort(Aeroportos);
	}
	
	public Aeroporto BuscarPorCodigoAeroporto(String x)
	{
		Aeroporto aux = null;
		
		for(Aeroporto ar : Aeroportos)
		{
			String s = ar.getCodigo();
			if(x.equals(s))
			{
				aux = ar;
			}
		}
		return aux;
	}
	
	public String toStringGerenciadorAeroportos()
	{
		String s = "";
		
		for(Aeroporto ar : Aeroportos)
		{
			s +=	String.format("%s\n", ar.toString());
		}
		
		return s;
	}
}
