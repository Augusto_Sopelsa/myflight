package pucrs.myflight.modelo;

public class Aeronave implements Imprimivel, Comparable<Aeronave>{
	private String codigo;
	private String descricao;
	private int capacidade;
	private static int total_aeronaves;
	
	public Aeronave(String codigo, String descricao, int capacidade) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.capacidade = capacidade;
		total_aeronaves ++;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public int getCapacidade() {
		return capacidade;
	}
	
	public static int getTotalAeronaves(){
		return total_aeronaves;
	}
	
	public String toString()
	{
		return String.format("%s\t %s\t %s\t", codigo, descricao, capacidade);
	}

	@Override
	public void imprimir() {
		System.out.println(codigo + "--" + descricao + "--" + capacidade);
		
	}

	@Override
	public int compareTo(Aeronave outra){
		return descricao.compareTo(outra.descricao);
	}
}
