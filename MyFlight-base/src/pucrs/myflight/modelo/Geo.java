package pucrs.myflight.modelo;

public class Geo {
	private double latitude;
	private double longitude;
	
	public Geo(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public static double distancia(Geo x, Geo y){
		double dist = 0.00;
		
		dist += 2*6371*Math.asin(Math.sqrt(Math.pow(Math.sin((Math.toRadians(x.getLatitude())
				- Math.toRadians(y.getLatitude())))/2, 2) + Math.pow(Math.sin((Math.toRadians(x.getLongitude()) 
				- Math.toRadians(y.getLongitude()))/2),2) * Math.cos(Math.toRadians(x.getLatitude())) 
				* Math.cos(Math.toRadians(y.getLatitude()))));	
		
		return dist;
	}
	
	public String toString(){
		return String.format("geoloc: (%f, %f)",latitude, longitude);
	}
}
