package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorCias {
	private ArrayList<CiaAerea> empresas;
	
	public GerenciadorCias()
	{
		empresas = new ArrayList<CiaAerea>();
	}
	
	public void AdicionarCia(CiaAerea x)
	{
		empresas.add(x);
	}
	
	public ArrayList<CiaAerea> ListarTodasCias()
	{
		return empresas; //teste do bitbucket
	}
	
	public CiaAerea BuscarPorCodigoCia(String x)
	{
		CiaAerea aux = null;
		
		for(CiaAerea ca : empresas)
		{
			String s = ca.getCodigo();
			if(x.equals(s))
			{
				aux = ca;
			}
		}
		return aux;
	}
	
	public CiaAerea BuscarPorNomeCia(String x)
	{
		CiaAerea aux = null;
		for(CiaAerea ca : empresas)
		{
			String s = ca.getNome();
			if(x.equals(s))
			{
				aux = ca;
			}
		}
		return aux;
	}
	
	public String toStringGerenciadorCias()
	{
		String s = "";
		
		for(CiaAerea cia : empresas)
		{
			s +=	String.format("%s\n",cia.toString());
		}
		
		return s;
	}
}
