package pucrs.myflight.modelo;

import java.util.ArrayList;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class GerenciadorVoos {
	private ArrayList<Voo> Voos;
	
	public GerenciadorVoos()
	{
		Voos = new ArrayList<Voo>();
	}

	public void AdicionarVoos(Voo x)
	{
		Voos.add(x);
	}

	public ArrayList<Voo> ListarTodosVoos()
	{
		return Voos;
	}
	
	public void OrdenaDataHora(){
		Voos.sort(Comparator.comparing(Voo::getDatahora));
	}
	
	public void OrdenaDataHoraDuracao(){
		Voos.sort(Comparator.comparing(Voo::getDatahora).thenComparing(Voo::getDuracao));
	}
	
	public ArrayList<Voo> BuscarPorDataVoos(LocalDateTime data)
	{  
		ArrayList<Voo> voo_data = new ArrayList<>();
		
		for(Voo v : Voos){
			LocalDate ld = v.getDatahora().toLocalDate();
			
			if(data.toLocalDate().equals(ld))
			{
				voo_data.add(v);
			}
		}
		return voo_data;
	}
	
	public String toString()
	{
		String s = "";
		
		for(Voo v : Voos)
		{
			s +=	String.format("%s\n", v.toString());
		}
		
		return s;
	}
}
