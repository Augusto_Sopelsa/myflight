package pucrs.myflight.modelo;

import java.util.Comparator;

public class Rota implements Comparable<Rota>,Comparator<Rota>{
	private CiaAerea cia;
	private Aeroporto origem;
	private Aeroporto destino;
	private Aeronave aeronave;
	
	public Rota(CiaAerea cia, Aeroporto origem, Aeroporto destino, Aeronave aeronave) {
		this.cia = cia;
		this.origem = origem;
		this.destino = destino;
		this.aeronave = aeronave;		
	}	
	
	public CiaAerea getCia() {
		return cia;
	}
	
	public Aeroporto getDestino() {
		return destino;
	}
	
	public Aeroporto getOrigem() {
		return origem;
	}
	
	public Aeronave getAeronave() {
		return aeronave;
	}
	
	public String toString()
	{
		return String.format("%s\t %s\t %s\t %s\t", cia.toString(), origem.toString(), destino.toString(), aeronave.toString());
	}

	@Override
	public int compareTo(Rota outra) {
		return cia.toString().compareTo(outra.cia.toString());
	}

	@Override
	public int compare(Rota r1, Rota r2) {
		return r1.origem.compareTo(r2.origem);
	}
}
