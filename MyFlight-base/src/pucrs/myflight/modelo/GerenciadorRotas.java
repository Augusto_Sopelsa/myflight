package pucrs.myflight.modelo;

import java.util.ArrayList;
import java.util.Comparator;

public class GerenciadorRotas {
	private ArrayList<Rota> Rotas;
	
	public GerenciadorRotas()
	{
		Rotas = new ArrayList<Rota>();
	}
	
	public void AdicionarRotas(Rota x)
	{
		Rotas.add(x);
	}

	public ArrayList<Rota> ListarTodasRotas()
	{
		return Rotas;
	}
	
	public void OrdenaCia(){
		Rotas.sort((Rota r1, Rota r2) -> r1.getCia().compareTo(r2.getCia()));
	}
	
	public void OrdenaOrigem(){
		Rotas.sort((Rota r1, Rota r2) -> r1.getOrigem().compareTo(r2.getOrigem()));
	}
	
	public void OrdenaOrigemCia(){
		Rotas.sort(Comparator.comparing(Rota::getOrigem).thenComparing(Rota::getCia));
	}
		
	public ArrayList<Rota> BuscarPorOrigemRota(Aeroporto x)
	{
		ArrayList<Rota> origem = new ArrayList<>();
		
		for(Rota r : Rotas)
		{
			Aeroporto ap = r.getOrigem();
			if(x.equals(ap))
			{
				origem.add(r);
			}
		}
		return origem;
	}
	
	public String toString()
	{
		String s = "";
		
		for(Rota rt : Rotas)
		{
			s +=	String.format("%s\n", rt.toString());
		}
		
		return s;
	}
}
