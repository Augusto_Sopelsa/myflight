package pucrs.myflight.modelo;

import java.time.LocalDateTime;

public class App {

	public static void main(String[] args) {
		
		//Geo Registers
		Geo geo1 = new Geo(-29.9939,-51.1711);
		Geo geo2 = new Geo(-23.4356,-46.4731);
		Geo geo3 = new Geo(38.7742,-9.1342);
		Geo geo4 = new Geo(25.7933,-80.2906);
		
		//Airplanes Registers
		Aeronave Aero1 = new Aeronave("733", "Boeing 737-300", 140);
		Aeronave Aero2 = new Aeronave("73G","Boeing 737-700",126);
		Aeronave Aero3 = new Aeronave("380","Airbus Industrie A380",644);
		Aeronave Aero4 = new Aeronave("764","Boeing 767-400",304);
		
		//Airlines Registers
		CiaAerea Cia1 = new CiaAerea("JJ","LATAM Linhas A�reas");
		CiaAerea Cia2 = new CiaAerea("G3","GOL Linhas A�reas SA");
		CiaAerea Cia3 = new CiaAerea("TP","TAP Portugal");
		CiaAerea Cia4 = new CiaAerea("AD","Azul Linhas A�reas");
		
		//Airports Registers
		Aeroporto Porto1 = new Aeroporto("POA","Salgado Filho Intl Apt",geo1);
		Aeroporto Porto2 = new Aeroporto("GRU","S�o Pualo Guarulhos Intl Apt",geo2);
		Aeroporto Porto3 = new Aeroporto("Lis","Lisbon",geo3);
		Aeroporto Porto4 = new Aeroporto("MIA","Miami International Apt",geo4);
		
		//Routes Registers
		Rota R1 = new Rota(Cia2,Porto2,Porto1,Aero1);//Sao paulo --> Porto Alegre
		Rota R2 = new Rota(Cia2,Porto1,Porto2,Aero2);//Porto Alegre --> Sao paulo
		Rota R3 = new Rota(Cia3,Porto4,Porto3,Aero3);//Miami --> Lisboa
		Rota R4 = new Rota(Cia1,Porto2,Porto4,Aero4);//Sao paulo --> Miami
		Rota R5 = new Rota(Cia4,Porto1,Porto2,Aero2);//Porto Alegre --> S�o paulo
		
		//date, hour and duration registers
		LocalDateTime datahora1 = LocalDateTime.of(2017, 5, 3, 16, 30);
		LocalDateTime datahora3 = LocalDateTime.of(2017, 4, 1, 10, 45);
		LocalDateTime datahora4 = LocalDateTime.of(2017, 8, 9, 23, 50);
		LocalDateTime datahora5 = LocalDateTime.of(2017, 2, 25, 6, 20);
		
		//Flights Registers
		VooDireto v1 = new VooDireto(datahora3, R1);
		VooDireto v2 = new VooDireto(datahora4, R3);
		VooDireto v3 = new VooDireto(datahora5, R4);
		VooEscalas v4 = new VooEscalas(datahora4);
		VooEscalas v5 = new VooEscalas(datahora1);
		
		//Gerenciador de Aeronaves
		GerenciadorAeronaves gnave = new GerenciadorAeronaves();
		gnave.AdicionarAeronave(Aero1);
		gnave.AdicionarAeronave(Aero2);
		gnave.AdicionarAeronave(Aero3);
		gnave.AdicionarAeronave(Aero4);
		
		//Gerenciador de Aeroportos
		GerenciadorAeroportos gporto = new GerenciadorAeroportos();
		gporto.AdicionarAeroportos(Porto1);
		gporto.AdicionarAeroportos(Porto2);
		gporto.AdicionarAeroportos(Porto3);
		gporto.AdicionarAeroportos(Porto4);
		
		//Gerenciador de CIAS
		GerenciadorCias gcias = new GerenciadorCias();
		gcias.AdicionarCia(Cia1);
		gcias.AdicionarCia(Cia2);
		gcias.AdicionarCia(Cia3);
		gcias.AdicionarCia(Cia4);
		
		//Gerenciador de Rotas
		GerenciadorRotas grotas = new GerenciadorRotas();
		grotas.AdicionarRotas(R1);
		grotas.AdicionarRotas(R2);
		grotas.AdicionarRotas(R3);
		grotas.AdicionarRotas(R4);
		grotas.AdicionarRotas(R5);
		
		//Gerenciador de Voos
		GerenciadorVoos gv = new GerenciadorVoos();
		gv.AdicionarVoos(v1);
		gv.AdicionarVoos(v2);
		gv.AdicionarVoos(v3);
		gv.AdicionarVoos(v4);
		gv.AdicionarVoos(v5);
		
		//Adiciona escalas em algum voo
		v4.adicionaEscala(R1);
		v4.adicionaEscala(R2);
		v4.adicionaEscala(R3);
		v4.adicionaEscala(R4);
		v4.adicionaEscala(R5);
		
		//Prints
		/*Mostra a duracao do voo*/	System.out.println(v1.getDuracao());		
		/*Mostra a duracao do voo com escalas*/	System.out.println(v4.getDuracao());
		/*Mostra o total de aeronaves cadastradas*/	System.out.println("Total de Aeronaves = " + Aeronave.getTotalAeronaves());
		/*Mostra todos os voos*/	System.out.println(gv.ListarTodosVoos());
		/*Ordena Aeronaves pela descricao*/	gnave.ordenaAeronaves();
		/*Mostra as aeronaves*/	System.out.println(gnave);
		/*Ordena as rotas pela compania*/	grotas.OrdenaCia();
		/*Mostra as rotas*/		System.out.println(grotas);
		/*Ordena as rotas pela origem*/		grotas.OrdenaOrigem();
		/*Mostra as rotas*/		System.out.println(grotas);
		/*Ordena as rotas pela origem e como desempate pela Compania*/		grotas.OrdenaOrigemCia();
		/*Mostra as rotas*/		System.out.println(grotas);
		/*Ordena os voos pela Data e Hora*/		gv.OrdenaDataHora();
		/*Mostra os voos*/		System.out.println(gv);
		/*Ordena os voos pela Data e Hora e como desempate pela duracao*/		gv.OrdenaDataHoraDuracao();
		/*Mostra os voos*/		System.out.println(gv);
	}
}	