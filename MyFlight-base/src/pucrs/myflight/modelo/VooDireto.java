package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;

public class VooDireto extends Voo{
	private Rota rota;
	
	public VooDireto(LocalDateTime hora, Rota rota){
		super(hora);
		this.rota = rota;
	}

	@Override
	public Rota getRota() {
		return rota;
	}

	@Override
	public Duration getDuracao() {
		double dist = 0.00;
		Duration t;
		
		dist = Geo.distancia(rota.getOrigem().getLocal(), rota.getDestino().getLocal());
	
		t = Duration.ofSeconds((long) ((((dist/805)+0.5) * 3600)));
		return t;
	}
	
	public String toString()
	{
		return super.toString() + rota.toString() + getDuracao();
	}
}