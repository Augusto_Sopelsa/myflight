package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VooEscalas extends Voo{
	private ArrayList<Rota> Escalas;
	private Rota rota;
    
	// Constructor
    public VooEscalas(LocalDateTime datahora)
    {
    	super(datahora);//call super constructor
    	Escalas = new ArrayList<Rota>();
    }
    
    public void adicionaEscala(Rota rota){
    	Escalas.add(rota);
    }
    
    public ArrayList<Rota> getRotas(){
    	return Escalas;
    }

	@Override
	public Rota getRota() {
		return rota;
	}

	@Override
	public Duration getDuracao() {
		double dist = 0.00;
		Duration t;
		for(Rota r: Escalas){
			dist += Geo.distancia(r.getOrigem().getLocal(), r.getDestino().getLocal());
		}
		t = Duration.ofSeconds((long) ((((dist/805)+0.5) * 3600)));
		return t;
	}

	@Override
    public String toString() {
		String s = "";
		
		for(Rota r : Escalas)
		{
			s +=	String.format("%s\t\n", r.toString()  + getDuracao());
		}
		
		return super.toString() + s;
    }

}