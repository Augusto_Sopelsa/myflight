package pucrs.myflight.modelo;
import java.util.ArrayList;
import java.util.Collections;

public class GerenciadorAeronaves {
	private ArrayList<Aeronave> Aeronaves;
	
	public GerenciadorAeronaves()
	{
		Aeronaves = new ArrayList<Aeronave>();
	}

	public void AdicionarAeronave(Aeronave x)
	{
		Aeronaves.add(x);
	}
	
	public ArrayList<Aeronave> ListarTodasAeronaves()
	{
		return Aeronaves;
	}
	
	public void ordenaAeronaves(){
		Collections.sort(Aeronaves);
	}
	
	public Aeronave BuscarPorCodigoAeronave(String x)
	{
		Aeronave aux = null;
		
		for(Aeronave ar : Aeronaves)
		{
			String s = ar.getCodigo();
			if(x.equals(s))
			{
				aux = ar;
			}
		}
		return aux;
	}
	
	public String toString()
	{
		String s = "";
		
		for(Aeronave an : Aeronaves)
		{
			s += String.format("%s\n", an.toString());
		}
		
		return s;
	}
}
